# gst-matrix

Python cli tool for audio/video streaming (calling) using Gstreamer with Matrix for signaling.

sudo apk add py3-gst gst-plugins-ugly gst-plugins-bad gst-plugins-good gst-plugins-base gstreamer-tools libnice libnice-gstreamer

To place a call user has to be at least Moderator level in the room.
