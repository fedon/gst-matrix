#!/usr/bin/python
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
import os
import sys
import signal
import threading
import json
import argparse
import importlib.util
import matrix_client.errors
from time import sleep
from time import time
from matrix_client.client import MatrixClient
from matrix_client.api import MatrixHttpApi
from pathlib import Path
from lmservice.lmservice import LocalMatrixService

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
gi.require_version('GstWebRTC', '1.0')
from gi.repository import GstWebRTC
gi.require_version('GstSdp', '1.0')
from gi.repository import GstSdp
PIPELINE_VO_DESC = '''
 v4l2src device=/dev/video2 ! video/x-bayer,format=bggr,width=720,height=1280,framerate=15/1 !
 bayer2rgb ! videoconvert ! queue ! vp8enc deadline=1 ! rtpvp8pay !
 queue ! application/x-rtp,media=video,encoding-name=VP8,payload=%s ! sendrecv.'''
PIPELINE_VO1_DESC = '''
 v4l2src device=/dev/video1 ! videoconvert ! queue ! vp8enc deadline=1 ! rtpvp8pay !
 queue ! application/x-rtp,media=video,encoding-name=VP8,payload=%s ! sendrecv.'''
PIPELINE_VOG_DESC = '''
 videotestsrc is-live=true pattern=ball ! videoconvert ! queue ! vp8enc deadline=1 ! rtpvp8pay !
 queue ! application/x-rtp,media=video,encoding-name=VP8,payload=%s ! sendrecv.'''

PIPELINE_DESC = '''
webrtcbin name=sendrecv bundle-policy=max-bundle stun-server=stun://stun.l.google.com:19302%s%s
'''
PIPELINE_PCMA_DESC = '''
 alsasrc ! alawenc ! rtppcmapay !
 queue ! application/x-rtp,media=audio,encoding-name=PCMA,payload=8 ! sendrecv.'''
PIPELINE_PCMU_DESC = '''
 alsasrc ! mulawenc ! rtppcmupay !
 queue ! application/x-rtp,media=audio,encoding-name=PCMU,payload=0 ! sendrecv.'''
PIPELINE_OPUS_DESC = '''
 alsasrc ! audioconvert ! audioresample ! queue ! opusenc ! rtpopuspay !
 queue ! application/x-rtp,media=audio,encoding-name=OPUS,payload=%s ! sendrecv.'''

CALL_EVENT_GROUP =      'm.call.'
CALL_EVENT_TYPE =       'm.call.invite'
ANSWER_EVENT_TYPE =     'm.call.answer'
CANDIDATES_EVENT_TYPE = 'm.call.candidates'
HANGUP_EVENT_TYPE =     'm.call.hangup'

RUN_TIME_PATH='/.local/run/matrix'
INVITE_DIR='/invite/'
CALL_DIR='/call/'

class MatrixGstClient:
    def __init__(self):
        self.pipe = None
        self.webrtc = None
        self.lock = threading.Lock()
        self.candidates =[]
        self.remote_ice = []
        self.history_limit = 30
        self.offer_set = False

    def get_room_id(self):
        if args.room_id is not None:
            room_id = args.room_id
            ex = room_id.index('!')
            if ex > 0:
                room_id = room_id[ex:]
            return room_id
        elif args.room_alias is not None:
            return self.api.get_room_id(args.room_alias)
        else : return None

    def hangup(self):
        print ('Hanging up...')
        msg = {'call_id': self.call_id}
        lms.send(msg, HANGUP_EVENT_TYPE, room_id)
        sleep(2)
        lms.close()
        # TODO implement mic turn off
        self.close_pipeline()

    def terminate(self, a, b):
        print("\nexiting...")
        if a == 2:
            reason = 'by user'
        elif a == 15:
            reason = 'killed'
        else:
            reason = 'unexpected ' + str(b)
        print("reason: " + reason)
        self.hangup()

    def send_sdp_answer(self, answer):
        text = answer.sdp.as_text()
        print ('Sending answer:\n%s' % text, flush=True)
        msg = {'call_id': self.call_id, 'answer': {'type': 'answer', 'sdp': text}, 'version': 0}
        lms.send(msg, ANSWER_EVENT_TYPE, room_id)

# to be deleted after call sibling is ready
    def on_offer_created(self, promise, _, __):
        promise.wait()
        reply = promise.get_reply()
        offer = reply['offer']
        promise = Gst.Promise.new()
        self.webrtc.emit('set-local-description', offer, promise)
        promise.interrupt()

    def on_answer_created(self, promise, e, __):
        promise.wait()
        reply = promise.get_reply()
        answer = reply.get_value('answer')
        assert(answer is not None)
        promise = Gst.Promise.new()
        self.webrtc.emit('set-local-description', answer, promise)
        promise.interrupt()
        self.send_sdp_answer(answer)

    def on_offer_set(self, promise, element, __):
        self.offer_set = True
        print('offer is set')
        promise.wait()
        promise = Gst.Promise.new_with_change_func(self.on_answer_created, None, None)
        self.webrtc.emit('create-answer', None, promise)
        self.find_ice()

    def send_ice_candidate_message(self, _, mlineindex, candidate):
        every = False
        candidate = {'candidate': candidate, 'sdpMLineIndex': mlineindex}
        print ('candidate : %s' % candidate)
        with self.lock:
            self.candidates.append(candidate)
            #   TODO optimize leftover
            if every or len(self.candidates) >= 3 :
                every = True
                icemsg = {"call_id": self.call_id, 'candidates': self.candidates}
                print ('Sending candidates')
                lms.send(icemsg, CANDIDATES_EVENT_TYPE, room_id)
                self.candidates = []

    def on_incoming_decodebin_stream(self, _, pad):
        if not pad.has_current_caps():
            print (pad, 'has no caps, ignoring')
            return

        caps = pad.get_current_caps()
        assert (len(caps))
        s = caps[0]
        name = s.get_name()
        if name.startswith('video'):
            q = Gst.ElementFactory.make('queue')
            conv = Gst.ElementFactory.make('videoconvert')
            sink = Gst.ElementFactory.make('autovideosink')
            self.pipe.add(q, conv, sink)
            self.pipe.sync_children_states()
            pad.link(q.get_static_pad('sink'))
            q.link(conv)
            conv.link(sink)
        elif name.startswith('audio'):
            q = Gst.ElementFactory.make('queue')
            conv = Gst.ElementFactory.make('audioconvert')
            resample = Gst.ElementFactory.make('audioresample')
            sink = Gst.ElementFactory.make('autoaudiosink')
            self.pipe.add(q, conv, resample, sink)
            self.pipe.sync_children_states()
            pad.link(q.get_static_pad('sink'))
            q.link(conv)
            conv.link(resample)
            resample.link(sink)

    def on_incoming_stream(self, _, pad):
        if pad.direction != Gst.PadDirection.SRC:
            return
        decodebin = Gst.ElementFactory.make('decodebin')
        decodebin.connect('pad-added', self.on_incoming_decodebin_stream)
        self.pipe.add(decodebin)
        decodebin.sync_state_with_parent()
        self.webrtc.link(decodebin)

    def parse_offer(self, message):
        offer_message = message['offer']
        assert(offer_message['type'] == 'offer')
        sdp_message = offer_message['sdp']
        print ('Received offer:\n%s' % sdp_message)
        res, sdpmsg = GstSdp.SDPMessage.new()
        GstSdp.sdp_message_parse_buffer(bytes(sdp_message.encode()), sdpmsg)
        self.offer = GstWebRTC.WebRTCSessionDescription.new(GstWebRTC.WebRTCSDPType.OFFER, sdpmsg)
        assert(self.offer is not None)
        sdp = self.offer.sdp
        a = ''
        v = ''
        mn = sdp.medias_len()
        for i in range(mn):
            me = sdp.get_media(i)
            print('media: ', me.media)
#  TODO multiple codec choice
            man = me.attributes_len()
            for i in range(man):
                attr = me.get_attribute(i)
                if attr.key == 'rtpmap':
                    # some browsers put codecs in lower case (Firefox)
                    if me.media == 'audio' and (attr.value.find('opus') > 0 or attr.value.find('OPUS') > 0):
                        a = attr.value[:attr.value.find(' ')]
                    elif me.media == 'video' and attr.value.find('VP8') > 0:
                        v = attr.value[:attr.value.find(' ')]
        self.get_pipeline_desc(a, v)

    def get_call(self, CALL_PATH):
        path = CALL_PATH
        calls = os.listdir(path)
        if len(calls) == 1:
            f = open(path+calls[0], 'r')
            self.room_id = f.readline()
            self.call_id = calls[0]
            f.close()
            print('Call in room: ' + self.room_id)
            return self.room_id, self.call_id
        elif len(calls) > 1:
            print('Multiple calls feature is not implemented yet ;(')
        else:
            print("No active call.\nExiting...", flush=True)
        self.close_pipeline()

    def get_pipeline_desc(self, a, v):
        print('codecs: ',a,v)
        pipeline_desc = PIPELINE_DESC
        if a == '':
            audio_desc = ''
        else:
            if args.ha is None:
                # TODO audio codec quality choice
                # TODO check a == 0 ?
                # audio_desc = PIPELINE_PCMU_DESC
                audio_desc = PIPELINE_OPUS_DESC % a
            else:
                audio_desc = PIPELINE_OPUS_DESC % a
#  TODO choose codec/media to use
        if v == '':
            video_desc = ''
        else:
            video_desc = PIPELINE_VO_DESC % v
        pipeline_desc = pipeline_desc % (video_desc, audio_desc)
        print('pipeline_desc: '+pipeline_desc)
        self.start_pipeline(pipeline_desc)

    def start_pipeline(self, pipeline_desc):
        self.pipe = Gst.parse_launch(pipeline_desc)
        self.webrtc = self.pipe.get_by_name('sendrecv')
# found enough data to try to accept the call
        if os.path.exists(CALL_PATH+self.call_id):
            os.remove(CALL_PATH+self.call_id)
        self.webrtc.connect('on-ice-candidate', self.send_ice_candidate_message)
        self.webrtc.connect('pad-added', self.on_incoming_stream)
        self.pipe.set_state(Gst.State.PLAYING)
# only playing webrtcbin can produce answer
        self.set_offer_sdp()

    def set_offer_sdp(self):
        promise = Gst.Promise.new_with_change_func(self.on_offer_set, self.webrtc, None)
        self.webrtc.emit('set-remote-description', self.offer, promise)
        promise.interrupt()

    def handle_ice(self, msg):
        print('Ice incoming: ' + json.dumps(msg))
        for ice in msg['candidates']:
            try:
                candidate = ice['candidate']
                sdpmlineindex = ice['sdpMLineIndex']
                self.webrtc.emit('add-ice-candidate', sdpmlineindex, candidate)
            except:
                print('Invalid ice: '+json.dumps(ice))
                print('Ignoring...')

# to be deleted after call sibling is ready
    def handle_answer(self, msg):
        if 'answer' in msg:
            answer = msg['answer']
            assert(answer['type'] == 'answer')
            sdp = answer['sdp']
            print ('Received answer:\n%s' % sdp)
            res, sdpmsg = GstSdp.SDPMessage.new()
            GstSdp.sdp_message_parse_buffer(bytes(sdp.encode()), sdpmsg)
            answer = GstWebRTC.WebRTCSessionDescription.new(GstWebRTC.WebRTCSDPType.ANSWER, sdpmsg)
            promise = Gst.Promise.new()
            self.webrtc.emit('set-remote-description', answer, promise)
            promise.interrupt()

    def close_pipeline(self):
        print('exiting...', flush=True)
        if self.pipe is not None:
            self.pipe.set_state(Gst.State.NULL)
            self.pipe = None
            self.webrtc = None
        sleep(1)
        exit(0)

    def find_ice(self):
        print('Looking for ice...', flush=True)
        while len(self.remote_ice) > 0:
            ice = self.remote_ice.pop()
            self.handle_ice(ice)

def listen_handler(e):
    print('Error: '+str(e))
    sleep(1)

# echo should not come here (server is responsible)
def listen_callback(event):
    e_type = event['type']
    if e_type.startswith(CALL_EVENT_GROUP) and event['content']['call_id'] == mc.call_id:
        if e_type == HANGUP_EVENT_TYPE:
            mc.hangup()
        elif e_type == CANDIDATES_EVENT_TYPE:
            if mc.offer_set:
                mc.handle_ice(event['content'])
            else:
                mc.remote_ice.insert(0, event['content'])
        elif e_type == ANSWER_EVENT_TYPE:
            print('What a mess - caller sends ANSWER_EVENT.\nIgnoring.')
        elif e_type == CALL_EVENT_TYPE:
            mc.parse_offer(event['content'])
        else:
            print('{{' + event['type'] + '}}\n' + json.dumps(event['content']))
            mc.hangup()
            lms.close()

def check_plugins():
    needed = ["alsa", "opus", "vpx", "nice", "webrtc", "dtls", "srtp", "rtp",
              "rtpmanager", "videotestsrc", "audiotestsrc"]
    missing = list(filter(lambda p: Gst.Registry.get().find_plugin(p) is None, needed))
    if len(missing):
        print('Missing gstreamer plugins:', missing, flush=True)
        return False
    else:
        return True

if __name__=='__main__':
    Gst.init(None)
    if not check_plugins():
        sys.exit(12)
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", dest="server",
                        help="server to login to")
    parser.add_argument("-u", "--username", dest="username",
                        help="username to login with")
    parser.add_argument("-p", "--password", dest="password",
                        help="the password")
    parser.add_argument("-la", "--low-a", dest="la",
                        help="low quality audio (PCMU 8k) default")
    parser.add_argument("-ha", "--high-a", dest="ha",
                        help="high quality audio (OPUS 44k)")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-r", "--room-id", dest="room_id",
                                help='specify the room id')
    group.add_argument("-a", "--room-alias", dest="room_alias",
                                help='specify the room by room alias')
    args = parser.parse_args()

    tr_path = os.getenv('XDG_RUNTIME_DIR')
    if tr_path is not None:
        CALL_PATH = tr_path+'/matrix'+CALL_DIR
    else:
        home = os.getenv('HOME')
        if os.path.exists(home + RUN_TIME_PATH):
            CALL_PATH = home + RUN_TIME_PATH + CALL_DIR
        else:
            print('Call dir is missing!', flush=True)
            sys.exit(11)

    print('starting...', flush=True)
    mc = MatrixGstClient()
    signal.signal(signal.SIGINT, mc.terminate)

    print('CALL_PATH: '+CALL_PATH, flush=True)
    room_id, call_id = mc.get_call(CALL_PATH)
    lms = LocalMatrixService(call_id, listen_callback, False)
    lms.listen()
    sys.exit(0)
