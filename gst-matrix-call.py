#!/usr/bin/python
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
import os
import sys
import signal
import threading
import json
import argparse
import importlib.util
import matrix_client.errors
from time import sleep
from time import time
from matrix_client.client import MatrixClient
from matrix_client.api import MatrixHttpApi

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
gi.require_version('GstWebRTC', '1.0')
from gi.repository import GstWebRTC
gi.require_version('GstSdp', '1.0')
from gi.repository import GstSdp

"""v4l2src ! queue ! vp8enc ! rtpvp8pay !
    application/x-rtp,media=video,encoding-name=VP8,payload=96 ! 
    webrtcbin name=sendrecv"""
PIPELINE_DESC = '''
webrtcbin name=sendrecv bundle-policy=max-bundle stun-server=stun://stun.l.google.com:19302
 videotestsrc is-live=true pattern=ball ! videoconvert ! queue ! vp8enc deadline=1 ! rtpvp8pay !
 queue ! application/x-rtp,media=video,encoding-name=VP8,payload=97 ! sendrecv.
 alsasrc ! audioconvert ! audioresample ! queue ! opusenc ! rtpopuspay !
 queue ! application/x-rtp,media=audio,encoding-name=OPUS,payload=96 ! sendrecv.
'''
PIPELINE2_DESC = '''
webrtcbin name=sendrecv bundle-policy=max-bundle stun-server=stun://stun.l.google.com:19302
 videotestsrc is-live=true pattern=ball ! videoconvert ! queue ! vp8enc deadline=1 ! rtpvp8pay !
 queue ! application/x-rtp,media=video,encoding-name=VP8,payload=97 ! sendrecv.
 alsasrc ! alawenc ! rtppcmapay !
 queue ! application/x-rtp,media=audio,encoding-name=PCMA,payload=8 ! sendrecv.
'''
PIPELINE_VO1_DESC = '''
webrtcbin name=sendrecv bundle-policy=max-bundle stun-server=stun://stun.l.google.com:19302
 v4l2src device=/dev/video1 ! videoconvert ! queue ! vp8enc deadline=1 ! rtpvp8pay !
 queue ! application/x-rtp,media=video,encoding-name=VP8,payload=97 ! sendrecv.
 alsasrc ! audioconvert ! audioresample ! queue ! opusenc ! rtpopuspay !
 queue ! application/x-rtp,media=audio,encoding-name=OPUS,payload=96 ! sendrecv.
'''

CALL_EVENT_GROUP =      'm.call.'
CALL_EVENT_TYPE =       'm.call.invite'
ANSWER_EVENT_TYPE =     'm.call.answer'
CANDIDATES_EVENT_TYPE = 'm.call.candidates'
HANGUP_EVENT_TYPE =     'm.call.hangup'

class MatrixGstClient:
    def __init__(self, username, passwd, server):
        self.username = username
        self.pipe = None
        self.webrtc = None
        self.passwd = passwd
        self.server = server or 'https://matrix-client.matrix.org'
        self.call_id = "c"+str(time())
        self.lock = threading.Lock()
        self.candidates =[]

    def get_room_id(self):
        if args.room_id is not None:
            room_id = args.room_id
            ex = room_id.index('!')
            if ex > 0:
                room_id = room_id[ex:]
            return room_id
        elif args.room_alias is not None:
            return self.api.get_room_id(args.room_alias)
        else : return None

    def connect(self):
        print('Connecting...')
        while True:
            try:
                self.client = MatrixClient(server)
                token = self.client.login(username,password)
                self.api = MatrixHttpApi(server, token)
                self.room_id = self.get_room_id()
                self.room = self.client.rooms[self.room_id]
                print('Connected.')
                break
            except matrix_client.errors.MatrixHttpLibError:
                print('error connecting. retrying after 1 second...', file=sys.stderr)
                sleep(1)

    def terminate(self, a, b):
        print("\nexiting...")
        if a == 2:
            reason = 'by user'
        elif a == 15:
            reason = 'killed'
        else:
            reason = 'unexpected ' + str(b)
        print("reason: " + reason)
        self.hangup()

    def hangup(self):
        print ('Hanging up...')
        msg = {'call_id': self.call_id}
        self.api.send_state_event(self.room_id, HANGUP_EVENT_TYPE, msg)

    def send_sdp_offer(self, offer):
        text = offer.sdp.as_text()
        print ('Sending offer:\n%s' % text, flush=True)
        msg = {'call_id': self.call_id, 'lifetime': 80000, 'offer': {'type': 'offer', 'sdp': text}, 'version': 0}
        self.api.send_state_event(self.room_id, CALL_EVENT_TYPE, msg)

    def on_offer_created(self, promise, _, __):
        promise.wait()
        reply = promise.get_reply()
        offer = reply['offer']
        promise = Gst.Promise.new()
        self.webrtc.emit('set-local-description', offer, promise)
        promise.interrupt()
        self.send_sdp_offer(offer)

    def on_negotiation_needed(self, element):
        promise = Gst.Promise.new_with_change_func(self.on_offer_created, element, None)
        element.emit('create-offer', None, promise)

    def send_ice_candidate_message(self, _, mlineindex, candidate):
        candidate = {'candidate': candidate, 'sdpMLineIndex': mlineindex}
        print ('candidate : %s' % candidate)
        with self.lock:
            self.candidates.append(candidate)
#   TODO handle leftover
            if len(self.candidates) >= 3 :
                icemsg = {"call_id": self.call_id, 'candidates': self.candidates}
                self.api.send_state_event(self.room_id, CANDIDATES_EVENT_TYPE, icemsg)
                self.candidates = []
                print ('Sending candidates')

    def on_incoming_decodebin_stream(self, _, pad):
        if not pad.has_current_caps():
            print (pad, 'has no caps, ignoring')
            return

        caps = pad.get_current_caps()
        assert (len(caps))
        s = caps[0]
        name = s.get_name()
        if name.startswith('video'):
            q = Gst.ElementFactory.make('queue')
            conv = Gst.ElementFactory.make('videoconvert')
            sink = Gst.ElementFactory.make('autovideosink')
            self.pipe.add(q, conv, sink)
            self.pipe.sync_children_states()
            pad.link(q.get_static_pad('sink'))
            q.link(conv)
            conv.link(sink)
        elif name.startswith('audio'):
            q = Gst.ElementFactory.make('queue')
            conv = Gst.ElementFactory.make('audioconvert')
            resample = Gst.ElementFactory.make('audioresample')
            sink = Gst.ElementFactory.make('autoaudiosink')
            self.pipe.add(q, conv, resample, sink)
            self.pipe.sync_children_states()
            pad.link(q.get_static_pad('sink'))
            q.link(conv)
            conv.link(resample)
            resample.link(sink)

    def on_incoming_stream(self, _, pad):
        if pad.direction != Gst.PadDirection.SRC:
            return

        decodebin = Gst.ElementFactory.make('decodebin')
        decodebin.connect('pad-added', self.on_incoming_decodebin_stream)
        self.pipe.add(decodebin)
        decodebin.sync_state_with_parent()
        self.webrtc.link(decodebin)

    def start_pipeline(self):
        self.pipe = Gst.parse_launch(PIPELINE_VO1_DESC)
        self.webrtc = self.pipe.get_by_name('sendrecv')
        self.webrtc.connect('on-negotiation-needed', self.on_negotiation_needed)
        self.webrtc.connect('on-ice-candidate', self.send_ice_candidate_message)
        self.webrtc.connect('pad-added', self.on_incoming_stream)
        self.pipe.set_state(Gst.State.PLAYING)

# the last method
    def handle_sdp(self, msg):
        print('SDP: ' + json.dumps(msg))
        if 'answer' in msg:
            answer = msg['answer']
            assert(answer['type'] == 'answer')
            sdp = answer['sdp']
            print ('Received answer:\n%s' % sdp)
            res, sdpmsg = GstSdp.SDPMessage.new()
            GstSdp.sdp_message_parse_buffer(bytes(sdp.encode()), sdpmsg)
            answer = GstWebRTC.WebRTCSessionDescription.new(GstWebRTC.WebRTCSDPType.ANSWER, sdpmsg)
            promise = Gst.Promise.new()
            self.webrtc.emit('set-remote-description', answer, promise)
            promise.interrupt()

    def handle_ice(self, msg):
        print('Ice: ' + json.dumps(msg))
        for ice in msg['candidates']:
            candidate = ice['candidate']
            sdpmlineindex = ice['sdpMLineIndex']
            self.webrtc.emit('add-ice-candidate', sdpmlineindex, candidate)

    def close_pipeline(self):
        self.pipe.set_state(Gst.State.NULL)
        self.pipe = None
        self.webrtc = None
        self.client.logout()
        sleep(1)
        exit(0)

    def loop(self):
        print('Listening')
        self.client.add_listener(listen_callback)
        self.client.listen_forever(exception_handler=listen_handler)
        print('...')

def listen_handler(e):
    print('Error: '+str(e))
    sleep(1) # for now we just wait TODO log

# TODO cleanup output
def listen_callback(event):
    if event['type'].startswith(CALL_EVENT_GROUP):
        call_id = event['content']['call_id']
        if event['type'] == HANGUP_EVENT_TYPE:
            if call_id == mc.call_id:
                mc.close_pipeline()
        elif event['type'] == ANSWER_EVENT_TYPE:
            if call_id == mc.call_id:
                mc.handle_sdp(event['content'])
        elif event['type'] == CANDIDATES_EVENT_TYPE:
            if event["sender"] == mc.client.user_id:
                print('Echo cadidates event')
            elif call_id == mc.call_id:
                mc.handle_ice(event['content'])
        elif event['type'] == CALL_EVENT_TYPE and event["sender"] == mc.client.user_id:
            print('Echo calling event')
        else:
            print('{{' + event['type'] + '}}\n' + json.dumps(event['content']))
            mc.hangup()

def check_plugins():
    needed = ["alsa", "opus", "vpx", "nice", "webrtc", "dtls", "srtp", "rtp",
              "rtpmanager", "videotestsrc", "audiotestsrc"]
    missing = list(filter(lambda p: Gst.Registry.get().find_plugin(p) is None, needed))
    if len(missing):
        print('Missing gstreamer plugins:', missing)
        return False
    return True

def load_config():
    home = os.getenv('HOME')
    if os.path.exists(home + '/.config/matrix/matrixcli.conf.py'):
        config_path = home + '/.config/matrix/matrixcli.conf.py'
    else:
        config_path = '/etc/matrix/matrixcli.conf.py'
#    else:
#        if os.path.exists(args.config):
#            if args.config.endswith('.py'):
#                config_path = args.config
#            else:
#                print("config file must be python file ends with .py", file=sys.stderr)
#                exit(1)
#        else:
#            print("the specified config file does not exist", file=sys.stderr)
#            exit(1)

    try:
        spec = importlib.util.spec_from_file_location("matrixcli.conf.py", config_path)
        config = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(config)
        try:
            config.ignore_rooms
        except AttributeError:
            config.ignore_rooms = []

    except FileNotFoundError:
        print("WARNING: config file does not exist", flush=True)
    return config

# Provides cli/config flexibility 
def config_vs_options(config):
    try :
        # if there is a configuration file
        # the default is to use the first account

        server = config.accounts[0]["server"]
        username = config.accounts[0]["username"]
        password = config.accounts[0]["passeval"]()

        if args.username is not None:
            # if username specified use it
            username = args.username

            indexes = [ index for index, account in enumerate(config.accounts) if account['username']==args.username]
            occurences = len(indexes)
            if occurences == 1:
                server = config.accounts[indexes[0]]['server']
                password = config.accounts[indexes[0]]['passeval']()
            elif occurences == 0:
                # if this username does not appear in the config file require server and password
                if args.password is None or args.server is None:
                    print("there is no username {0} in the config file,\n".format(args.username) +
                          "in this case you have to pass the password and server through the command line options", file=sys.stderr)
                    exit(1)
                else:
                    password = args.password
                    server = args.server
            else:
                # occurences are more than one
                if args.server is None:
                    # if the user didn't specify the server choose the first account
                    server = config.accounts[indexes[0]]["server"]
                    password = config.accounts[indexes[0]]["passeval"]()
                else:
                    for index in indexes:
                        if config.accounts[index]["server"] == args_server:
                            server = args_server
                            password = config.accounts[index]["passeval"]()
                            break

            if args.password is not None:
                # if the user specified a password use it and neglect any configuration password
                password = args.password
        else:
            if args.password is not None or args.server is not None:
                print("you can't specify password or server without username", file=sys.stderr)
                exit(1)

        return server, username, password
    except AttributeError as err:
        # if there is no configuration file require the three command line
        # arguments to be not None

        if all([args.server, args.password, args.username]):
            return args_server, args_username, args_password
        else :
            print("config file with accounts list does not exist, you have to specify the --server, --username and --password \n" +
                  "error raised: {0}".format(err), file=sys.stderr)
            exit(1)


if __name__=='__main__':
    Gst.init(None)
    if not check_plugins():
        sys.exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", dest="server",
                        help="server to login to")
    parser.add_argument("-u", "--username", dest="username",
                        help="username to login with")
    parser.add_argument("-p", "--password", dest="password",
                        help="the password")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-r", "--room-id", dest="room_id",
                                help='specify the room id')
    group.add_argument("-a", "--room-alias", dest="room_alias",
                                help='specify the room by room alias')
    args = parser.parse_args()
    server, username, password = config_vs_options(load_config())

    mc = MatrixGstClient(username, password, server)
    signal.signal(signal.SIGINT, mc.terminate)

    mc.connect()
    mc.start_pipeline()
    mc.loop()
    sys.exit(0)
